﻿using System;
using Dziedziczenie;
using System.Collections.Generic;
using System.Text;

namespace PK043585
{
    class BMW : IPojazd
    {
        public Silnik Silnik { get; private set; }

        public void Jedz() => Jedz(250);

        public void Jedz(int dystans)
        {
            for (int i = 0; i < dystans; i++)
            {
                if (dystans > 240 & i == 245)
                {
                    Console.WriteLine("\nError 404");
                    break;
                }
                if (i % 25 == 1) { Console.WriteLine("gaz"); }
                Silnik.Dzialaj();
            }
             Console.WriteLine("\nStahp!"); 
        }

        public BMW(V8 silnik)
        {
            Silnik = silnik;
        }
    }
}
