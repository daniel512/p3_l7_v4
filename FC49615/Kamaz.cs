using Dziedziczenie;

namespace FC49615
{
    public class Kamaz : IPojazd
    {
        public Silnik SilnikHybrydowy { get; private set; }
        public Silnik SilnikBenzynowy { get; private set; }

        public void Jedz(int dystans)
        {
            for (int i = 0; i < dystans; i++)
            {
                SilnikHybrydowy.Dzialaj();
                SilnikBenzynowy.Dzialaj();
            }
        }

        public void Jedz() => Jedz(100);

        public Kamaz(SilnikElektryczny hybrid, SilnikBenzynowy benzynowy)
        {
            SilnikHybrydowy = hybrid;
            SilnikBenzynowy = benzynowy;
        }
        
    }
}