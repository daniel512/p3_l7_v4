﻿using Dziedziczenie;
using System;
using System.Collections.Generic;
using System.Text;

namespace DK54423
{
    class Osobowy : IPojazd
    {
        public Silnik Silnik { get; set; }

        public int PojemnoscBaku { get; set; }

        public double IloscPaliwa { get; set; }

        public int Predkosc { get; set; }

        public int PredkoscMax { get; set; }

        public int Przyspieszenie { get; set; }

        public int Przebieg { get; set; }

        public void Jedz()
        {
            while (IloscPaliwa > 0)
            {

                if (Predkosc < PredkoscMax)
                {
                    Predkosc += Przyspieszenie;
                }


                Silnik.Dzialaj();

                Console.WriteLine($"Aktualna predkosc: {Predkosc} |  Ilosc paliwa: {IloscPaliwa}");


                this.IloscPaliwa -= this.Silnik.Spalanie * 0.1;
            }

            while (this.IloscPaliwa < 1 && this.Predkosc > 1)
            {
                Predkosc -= 3;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Aktualna predkosc: {Predkosc} |  Ilosc paliwa: {IloscPaliwa}");
            }


        }

        public void Jedz(int dystans)
        {
            throw new NotImplementedException();
        }

        public Osobowy(Silnik silnik, int pojemnoscBaku, int przyspieszenie, int predkoscMax)
        {
            Silnik = silnik;
            IloscPaliwa = pojemnoscBaku;
            Przyspieszenie = przyspieszenie;
            PredkoscMax = predkoscMax;
        }
    }
}
